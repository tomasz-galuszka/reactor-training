package com.galuszkat.training.reactor;

import reactor.core.publisher.Flux;

import java.util.function.Consumer;

import org.reactivestreams.Subscription;

public class FluxBaseSubscription {

  public static void run() {

    Consumer<String> consumer = s -> System.out.println("Consumed: " + s);
    Consumer<Throwable> errorHandler = e -> System.err.println("Error occurred: " + e.getMessage());
    Consumer<Subscription> subscriptionConsumer = s -> s.request(2); // request only 2 elements
    Runnable completionHandler = () -> System.out.println("Completed.");

    Flux.just("A", "B", "C", "D")
        .map(letter -> {
          if (letter.equalsIgnoreCase("C")) {
            throw new RuntimeException("Letter c not supported");
          }
          return letter;
        })
        .subscribe(consumer, errorHandler, completionHandler, subscriptionConsumer);

  }

}
